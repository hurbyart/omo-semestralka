import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.chainParticipants.Farmer;
import cz.cvut.fel.omo.foodChain.chainParticipants.VegetableFarmer;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import static org.junit.Assert.*;

public class VegetableFarmerTest {

    private Farmer farmer;
    private Request request;

    @Before
    public void setUp() {
        farmer = new VegetableFarmer(null, null, 0,
                                Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER), null);
        request = new Request(null, ONION, 1, OperationEnum.GROW);
    }

    @Test
    public void checkAskPrice() {
        int expectedPrice = 40;

        farmer.accept(new PriceWriter());
        int actualPrice = farmer.askPrice(ONION, 4);
        assertEquals(expectedPrice, actualPrice);
    }

    @Test
    public void checkIsAgreeToExecute() {
        boolean is = farmer.isAgreeToExecute(request);

        assert (is);
    }

    @Test
    public void checkProcess1() {
        FoodEnum expectedName = ONION;

        FoodEntity product = farmer.process(request);
        FoodEnum actualName = product.getName();
        assertEquals(expectedName, actualName);
    }

    @Test
    public void checkProcess2() {
        int expectedQuantity = 1;

        FoodEntity product = farmer.process(request);
        int actualQuantity = product.getQuantity();
        assertEquals(expectedQuantity, actualQuantity);
    }

}