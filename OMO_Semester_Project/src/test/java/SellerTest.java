import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.chainParticipants.Seller;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.*;

public class SellerTest {

    private Seller seller;
    private Request request;

    @Before
    public void setUp() {
        seller = new Seller("seller", 500, null);
        request = new Request(null, FoodEnum.MILK, 1, OperationEnum.SELL);
    }

    @Test
    public void checkAskPrice() {
        int expectedPrice = 3000;

        seller.accept(new PriceWriter());
        int actualPrice = seller.askPrice(FoodEnum.KYIVCUTLET, 10);
        assertEquals(expectedPrice, actualPrice);
    }

    @Test
    public void checkIsAgreeToExecute() {
        seller.addFood(new FoodEntity(FoodEnum.MILK, 1, 5));
        boolean is = seller.isAgreeToExecute(request);
        assert (is);
    }

    @Test
    public void checkProcess() {
        FoodEntity expectedProduct = new FoodEntity(FoodEnum.MILK, 1, 5);
        seller.addFood(new FoodEntity(FoodEnum.MILK, 1, 5));
        FoodEntity actualProduct = seller.process(request);
        assertEquals(expectedProduct.getName(), actualProduct.getName());
    }
}