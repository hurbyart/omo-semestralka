import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.BORSHCH;
import static org.junit.Assert.*;

public class MeatChannelTest {

    private Party meatFarmer;
    private Party manufacturer;
    private Party customer;
    private List<FoodEnum> meatIngredients = new ArrayList<>();
    private List<Party> participants = new ArrayList<>();

    @Before
    public void setUp() {
        Collections.addAll(meatIngredients, MILK, MEAT, BUTTER, EGGS);
        meatFarmer = new MeatFarmer("Tomas", "USA", 1000, meatIngredients, null);
        manufacturer = new Manufacturer("Tomas", 1000, BORSHCH, null);
        customer = new Customer("Julie", 1000, null);
        Collections.addAll(participants, meatFarmer, manufacturer, customer);
        MeatChannel.getMeatChannel(participants, meatIngredients, null);
        MeatChannel.getMeatChannel().register(meatFarmer);
        MeatChannel.getMeatChannel().register(manufacturer);
        MeatChannel.getMeatChannel().register(customer);
    }

    @Test
    public void register() {
        List<Party> expectedParticipants = new ArrayList<>();
        Collections.addAll(expectedParticipants, meatFarmer, manufacturer, customer);
        int expectedSize = 3;
        int actualSize = MeatChannel.getMeatChannel().getCurrentParticipants().size();
        assertEquals(expectedSize, actualSize);
    }
}