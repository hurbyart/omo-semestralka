import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import org.junit.Test;

import static org.junit.Assert.*;

public class PaymentChannelTest {

    @Test
    public void checkGetPaymentChannelSingleton() {
        PaymentChannel initialChannel = PaymentChannel.getPaymentChannel(null, null);
        PaymentChannel gottenChannel = PaymentChannel.getPaymentChannel();
        assertEquals(initialChannel, gottenChannel);
    }

}

