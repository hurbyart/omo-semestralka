package cz.cvut.fel.omo.foodChain.strategy;

import cz.cvut.fel.omo.foodChain.product.FoodEntity;

public interface ManufacturerStrategy {
    FoodEntity cook();
}
