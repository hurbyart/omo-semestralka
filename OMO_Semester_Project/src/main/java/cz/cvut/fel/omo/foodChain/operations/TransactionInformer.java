package cz.cvut.fel.omo.foodChain.operations;

import cz.cvut.fel.omo.foodChain.chainParticipants.Party;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.Main.logger;

public class TransactionInformer implements Observable {

    private List<Transaction> transactions = new ArrayList<>();
    private List<Party> parties = new ArrayList<>();
    private List<String> securityReports;


    public void attach(Party party) {                                       // a part of Observer pattern
        parties.add(party);
    }

    public void detach(Party party) {                                       // a part of Observer pattern
        parties.remove(party);
    }

    /**
     * uses to inform all parties about some action
     *
     * @param transaction
     */
    public void notify(Transaction transaction) {
        transactions.add(transaction);
        for (Party party : parties) {
            party.updateTransactions(transaction);
        }
    }

    public void addToSecurityReport(String report) {
        securityReports.add(report);
    }

    public void makeSecurityReport() {
        if (securityReports == null) System.out.println("Wasn't any double spending problem");
        else {
            for (String report : securityReports) {
                System.out.println(report);
            }
        }
    }

    public void makeTextSecurityReport() {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("SecurityReport.txt"), "utf-8"))) {
            writer.write("-------------------- SECURITY REPORT --------------------\n");
            if (securityReports == null) writer.write("Wasn't any double spending problem. \n");
            for (String report : securityReports) {
                writer.write(report + "\n");
            }
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
    }

    /**
     * uses to make all transactions report
     */
    public void makeReport() {
        System.out.println("-------------------- TRANSACTION INFORMER REPORT --------------------");
        for (Transaction transaction : this.transactions) {
            transaction.getTransactionInfo();
        }
        System.out.println("---------------------------------------------------------------------");
    }

    /**
     * uses to generate text report with all transactions
     */
    public void generateTextReport() {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("TransactionReport.txt"), "utf-8"))) {
            writer.write("-------------------- TRANSACTION INFORMER REPORT --------------------\n");
            for (Transaction transaction : this.transactions) {
                writer.write(transaction.getInfoForText() + "\n");
            }
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
    }
}
