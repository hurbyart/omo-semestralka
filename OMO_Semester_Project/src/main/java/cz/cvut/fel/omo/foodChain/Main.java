package cz.cvut.fel.omo.foodChain;

import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;

public class Main {

    public final static Logger logger = Logger.getLogger("Loher");

    public static void main(String[] args) {

        List<Party> participants = new ArrayList<>();
        TransactionInformer transactionInformer = new TransactionInformer();
        List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
        List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
        List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);

        // Create all participants
        MeatFarmer meatFarmer = new MeatFarmer("Igor Farmer", "Belarus", 1000, meatIngredients, transactionInformer);
        VegetableFarmer vegetableFarmer = new VegetableFarmer("Panas Farmer", "Ukraine", 2000, vegetableIngredients, transactionInformer);
        Manufacturer manufacturerBorshch = new Manufacturer("Dasha1", 1000, BORSHCH, transactionInformer);
        Manufacturer manufacturerDraniki = new Manufacturer("Dasha2", 1000, DRANIKI, transactionInformer);
        Manufacturer manufacturerIceCream = new Manufacturer("Dasha3", 1000, ICECREAM, transactionInformer);
        Manufacturer manufacturerKyivCutlet = new Manufacturer("Dashadsamayaluchshaya", 1000, KYIVCUTLET, transactionInformer);
        Manufacturer manufacturerPancakes = new Manufacturer("Dasha5", 1000, PANCAKES, transactionInformer);
        Storehouse storehouse = new Storehouse("Store", 1000, transactionInformer);
        Seller seller = new Seller("Artem Seller",1000, transactionInformer);
        Retailer retailer = new Retailer("Pasha Retailer",1000, transactionInformer);
        Customer customer1 = new Customer("Masha BolshieSiski", 400, transactionInformer);
        Customer customer2 = new Customer("Vitia Bomzh", 1000, transactionInformer);

        Collections.addAll(participants, meatFarmer, vegetableFarmer, manufacturerBorshch, manufacturerDraniki, manufacturerIceCream,
                manufacturerKyivCutlet, manufacturerPancakes, storehouse, seller, retailer, customer1, customer2);

        // Create channels
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);

        PriceWriter visitor = new PriceWriter();
        visitor.write(participants);

        // Register participants to the channel and attach them to the Transaction Informer
        for (Party participant : participants) {
            transactionInformer.attach(participant);
            participant.registerToTheChannel();
        }

        // STEP 1
        // Fill some participants with products
        manufacturerDraniki.sendRequest(POTATO, 2);
        storehouse.sendRequest(BORSHCH, 2);
        storehouse.sendRequest(MEAT, 1);
        storehouse.sendRequest(DRANIKI, 1);
        seller.sendRequest(KYIVCUTLET, 1);
        seller.sendRequest(POTATO, 2);
        retailer.sendRequest(BEET, 3);
        retailer.sendRequest(PANCAKES, 2);

        // STEP 2
        customer1.sendRequest(ICECREAM, 2);
        customer1.sendRequest(MILK, 3);
        customer2.sendRequest(DRANIKI, 1);
        customer2.sendRequest(PANCAKES, 2);
        storehouse.sendRequest(DRANIKI, 1);
        seller.sendRequest(POTATO, 2);
        retailer.sendRequest(KYIVCUTLET, 1);

        // STEP 3
        customer1.sendRequest(ICECREAM, 2);
        customer1.sendRequest(POTATO, 2);
        customer2.sendRequest(BORSHCH, 2);

//        System.out.println("CUSTOMER1 PRODUCTS");
//        for (FoodEntity foodEntity : customer1.getProducts()) {
//            System.out.println(foodEntity.getName());
//            foodEntity.writeAllActions();
//        }
//
//        System.out.println("CUSTOMER2 PRODUCTS");
//        for (FoodEntity foodEntity : customer2.getProducts()) {
//            System.out.println(foodEntity.getName());
//            foodEntity.writeAllActions();
//        }
//
//        System.out.println("SELLER PRODUCTS");
//        for (FoodEntity foodEntity : seller.getProducts()) {
//            System.out.println(foodEntity.getName());
//            foodEntity.writeAllActions();
//        }
//
        System.out.println("RETAILER PRODUCTS");
        for (FoodEntity foodEntity : retailer.getProducts()) {
            System.out.println(foodEntity.getName());
            foodEntity.writeAllActions();

        }

//        System.out.println("STOREHOUSE PRODUCTS");
//        for (FoodEntity foodEntity : storehouse.getProducts()) {
//            System.out.println(foodEntity.getName());
//            foodEntity.writeAllActions();
//        }
//
//        System.out.println("MANUFACTURER BORSHCH PRODUCTS");
//        for (FoodEntity foodEntity : manufacturerBorshch.getProducts()) {
//            System.out.println(foodEntity.getName());
//            foodEntity.writeAllActions();
//        }
//
        System.out.println("customer money : " + customer1.getCashAccount());


        generateTextPartiesReport(storehouse);

        transactionInformer.makeReport();
        transactionInformer.makeSecurityReport();
        transactionInformer.generateTextReport();
    }

    private static void generatePartiesReport(ChainParticipant chainParticipant){
        for(Iterator it = chainParticipant.getIterator(); it.hasNext(); ){
            Object text = it.next();
            if(text != null ) System.out.println(text);
        }
    }

    private static void generateTextPartiesReport(ChainParticipant chainParticipant){
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("PartyReport.txt"), "utf-8"))) {
            writer.write("-------------------- " + chainParticipant.getName() + " REPORT --------------------\n");
            for(Iterator it = chainParticipant.getIterator(); it.hasNext(); ){
                Object text = it.next();
                if(text != null) {
                    writer.write((String)text+ "\n");
                }
            }
        } catch (IOException e){
            logger.warning(e.getMessage());
        }
    }

}
