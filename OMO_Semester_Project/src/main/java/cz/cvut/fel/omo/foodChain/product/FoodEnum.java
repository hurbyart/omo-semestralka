package cz.cvut.fel.omo.foodChain.product;

public enum FoodEnum {
    PANCAKES,
    KYIVCUTLET,
    DRANIKI,
    ICECREAM,
    BORSHCH,
    MILK,
    MEAT,
    BUTTER,
    POTATO,
    ONION,
    EGGS,
    SUGAR,
    FLOUR,
    BEET,
    WATER
}
