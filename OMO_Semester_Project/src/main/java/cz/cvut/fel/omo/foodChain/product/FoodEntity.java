package cz.cvut.fel.omo.foodChain.product;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.chainParticipants.Party;
import cz.cvut.fel.omo.foodChain.states.FinalPackedState;
import cz.cvut.fel.omo.foodChain.states.State;
import cz.cvut.fel.omo.foodChain.states.TemporaryPackedState;
import cz.cvut.fel.omo.foodChain.states.UnpackedState;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.Main.logger;

public class FoodEntity {

    private FoodEnum name;
    private int quantity;
    private final int storageTemperature;
    private List<String> doneActions = new ArrayList<>();
    private State state;

    public FoodEntity(FoodEnum name, int quantity, int storageTemperature) {
        this.name = name;
        this.quantity = quantity;
        this.storageTemperature = storageTemperature;
        this.state = new UnpackedState(this);
    }

    public FoodEnum getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getStorageTemperature() {
        return storageTemperature;
    }

    public List<String> getDoneActions() {
        return doneActions;
    }

    public void addAction(String action) {
        doneActions.add(action);
    }

    /**
     * uses for product report
     */
    public void writeAllActions() {
        for (String action : doneActions) {
            System.out.println(action);
        }
    }

    public void writeAllActionsText() {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(this + "Report.txt"), "utf-8"))) {
            writer.write("-------------------- FOODENTITY INFORMER REPORT --------------------\n");
            for (String action : doneActions) {
                writer.write(action + "\n");
            }
        } catch (IOException e) {
            logger.warning(e.getMessage());
        }
    }

    /**
     * uses for change food entity state to unpacked
     */
    public void useForCooking() {
        if (!state.cook()) state = new UnpackedState(this);
    }

    /**
     * if applicant is customer food entity will be packed in final packing,otherwise in temporary packing
     *
     * @param apllicant
     */
    public void transport(Party apllicant) {
        if (apllicant instanceof Customer) {
            if (!state.presentProductToCustomer()) state = new FinalPackedState(this);
        } else if (!state.transport()) state = new TemporaryPackedState(this);
    }

    public State getState() {
        return state;
    }

}
